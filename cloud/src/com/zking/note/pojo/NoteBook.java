package com.zking.note.pojo;

public class NoteBook {
	private String bookId;
	private String userId;
	private String typeId;
	private String bookName;
	private String bookNick;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookNick() {
		return bookNick;
	}

	public void setBookNick(String bookNick) {
		this.bookNick = bookNick;
	}

}
