package com.zking.note.pojo;

public class Note {
	private String notesId;
	private String book_id;
	private String user_id;
	private String state_id;
	private String type_id;
	private String notes_tite;
	private String notes_content;
	private String notes_createtime;
	private String notes_updatetime;

	public String getNotesId() {
		return notesId;
	}

	public void setNotesId(String notesId) {
		this.notesId = notesId;
	}

	public String getBook_id() {
		return book_id;
	}

	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getState_id() {
		return state_id;
	}

	public void setState_id(String state_id) {
		this.state_id = state_id;
	}

	public String getType_id() {
		return type_id;
	}

	public void setType_id(String type_id) {
		this.type_id = type_id;
	}

	public String getNotes_tite() {
		return notes_tite;
	}

	public void setNotes_tite(String notes_tite) {
		this.notes_tite = notes_tite;
	}

	public String getNotes_content() {
		return notes_content;
	}

	public void setNotes_content(String notes_content) {
		this.notes_content = notes_content;
	}

	public String getNotes_createtime() {
		return notes_createtime;
	}

	public void setNotes_createtime(String notes_createtime) {
		this.notes_createtime = notes_createtime;
	}

	public String getNotes_updatetime() {
		return notes_updatetime;
	}

	public void setNotes_updatetime(String notes_updatetime) {
		this.notes_updatetime = notes_updatetime;
	}

}
