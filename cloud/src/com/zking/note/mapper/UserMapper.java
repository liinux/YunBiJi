package com.zking.note.mapper;

import com.zking.note.pojo.User;

/**
 * mapper代理开发方式,相当于dao层
 * @author Administrator
 */
public interface UserMapper {
	/**
	 * 插入一个用户信息
	 * @param user
	 * @throws Exception
	 */
	public void addUser(User user) throws Exception;
	
	/**
	 * 根据id查询某个用户
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User findUserById(String id) throws Exception;
	
	/**
	 * 根据姓名查询
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public User findUserByName(String name) throws Exception;
}
