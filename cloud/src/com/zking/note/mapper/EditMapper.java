package com.zking.note.mapper;

import java.util.List;
import java.util.Map;

import com.zking.note.pojo.Note;
import com.zking.note.pojo.NoteBook;

public interface EditMapper {
	/**
	 * 根据id查询用户笔记本
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findEditById(String id) throws Exception;

	/**
	 * 根据book_id查询全部笔记
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findEditBookById(String id)
			throws Exception;

	/**
	 * 根据notes_id查询笔记内容
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Note findEditBookTxtById(String notes_id) throws Exception;

	/**
	 * 根据notes_id修改笔记内容
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public void updateEditBookTxtById(Note note) throws Exception;

	/**
	 * 根据user_id，book_id，notes_id来添加笔记
	 */
	public void addNotBook(Note note) throws Exception;
	/**
	 * 根据user_id来添加笔记本
	 */
	public void addNotBooks(NoteBook notebook) throws Exception;
}
