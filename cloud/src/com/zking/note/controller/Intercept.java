package com.zking.note.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Intercept implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		// 强转类型
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		// 获取到sesson里面的值
		String name = (String) req.getSession().getAttribute("name");
		// 获取到地址栏路径
		String str = req.getRequestURL().toString();
		// 判断地址栏路径后缀是否为log_in.html,如果是的话直接放行
		if (str.endsWith("log_in.html")) {
			chain.doFilter(req, resp);
			return;
		}
		// 如果sesson里面的值为空的话就重定向到登陆页面
		if (name == null) {
			resp.sendRedirect(req.getContextPath()+"/log_in.html");
			return;
		}
		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
