package com.zking.note.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zking.note.pojo.Note;
import com.zking.note.service.EditService;

@Controller
@Component
@RequestMapping("/edit")
public class EditController {
	@Resource
	private EditService editService;

	/*
	 * private Note note;
	 * 
	 * public void setNote(Note note) { this.note = note; }
	 */

	public void setEditService(EditService editService) {
		this.editService = editService;
	}

	@RequestMapping("/countnote")
	@ResponseBody
	public JsonResult<List<Map<String, Object>>> edit(String userId) {
		System.out.println(userId);
		try {
			List<Map<String, Object>> list = editService.Editslect(userId);
			return new JsonResult<List<Map<String, Object>>>(list);
		} catch (Exception e) {
			return new JsonResult<List<Map<String, Object>>>(e);
		}
	}

	@RequestMapping("/countnotetype")
	@ResponseBody
	public JsonResult<List<Map<String, Object>>> edittype(String book_id) {
		try {
			List<Map<String, Object>> booklist = editService
					.EditslectBook(book_id);
			return new JsonResult<List<Map<String, Object>>>(booklist);
		} catch (Exception e) {
			return new JsonResult<List<Map<String, Object>>>(e);
		}
	}

	@RequestMapping("/countnotetxt")
	@ResponseBody
	public JsonResult<Note> edittxt(String notes_id) {
		try {
			Note node = editService.EditslectBookTxt(notes_id);
			return new JsonResult<Note>(node);
		} catch (Exception e) {
			return new JsonResult<Note>(e);
		}
	}

	@RequestMapping("/updateNote")
	@ResponseBody
	public void updateNote(String txtid, String title, String body) {
		Note note = new Note();
		note.setNotesId(txtid);
		note.setNotes_tite(title);
		note.setNotes_content(body);
		try {
			editService.EditUpdateBookTxt(note);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/addNote")
	@ResponseBody
	public void addNote(String title, String book_id, String userId) {

		try {
			editService.EditaddNotBook(title, book_id, userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/addNoteBook")
	@ResponseBody
	public void addNoteBook(String title, String userId) {
		System.out.println(title + "," + userId);
		try {
			editService.EditaddNotBooks(userId, title);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
