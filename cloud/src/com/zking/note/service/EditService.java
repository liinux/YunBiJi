package com.zking.note.service;

import java.util.List;
import java.util.Map;

import com.zking.note.pojo.Note;

public interface EditService {
	/**
	 * 根据id查询用户笔记本
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> Editslect(String user_id) throws Exception;

	/**
	 * 根据book_id查询全部笔记
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> EditslectBook(String id) throws Exception;

	/**
	 * 根据notes_id查询笔记内容
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Note EditslectBookTxt(String notes_id) throws Exception;

	/**
	 * 根据notes_id修改笔记内容
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public void EditUpdateBookTxt(Note note) throws Exception;
	/**
	 * 根据user_id，book_id，notes_id来添加笔记
	 * @param notes_id
	 * @param user_id
	 * @param book_id
	 * @param notes_tite
	 * @throws Exception
	 */
	public void  EditaddNotBook(String title, String notebookId, String userId) throws Exception;
	/**
	 * 根据user_id来添加笔记本
	 * @param book_id
	 * @param user_id
	 * @param book_name
	 * @throws Exception
	 */
	public void  EditaddNotBooks(String user_id,String book_name) throws Exception;

}