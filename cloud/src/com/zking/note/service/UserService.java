package com.zking.note.service;

import com.zking.note.pojo.User;

/**
 * 业务层接口
 * @author Administrator
 */
public interface UserService{
	/**
	 * 用户登录的方法
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public User login(String username,String password) throws Exception;
	/**
	 * 用户注册的方法
	 * @param name
	 * @param password
	 * @param nick
	 * @return
	 * @throws Exception
	 */
	public User regist(String name, String password, String nick) throws Exception;
}
