package com.zking.note.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.zking.note.exception.ServiceException;
import com.zking.note.mapper.EditMapper;
import com.zking.note.pojo.Note;
import com.zking.note.pojo.NoteBook;

@Component
public class EditServiceImp implements EditService {
	@Resource
	private EditMapper editMapper;

	/*
	 * @Resource private Note note;
	 * 
	 * public void setNote(Note note) { this.note = note; }
	 */

	public void setEditMapper(EditMapper editMapper) {
		this.editMapper = editMapper;
	}

	@Override
	public List<Map<String, Object>> Editslect(String user_id) throws Exception {
		if (user_id == null || user_id.trim().isEmpty()) {
			throw new ServiceException("userId不能为空");
		}
		return editMapper.findEditById(user_id);

	}

	@Override
	public List<Map<String, Object>> EditslectBook(String id) throws Exception {
		if (id == null || id.trim().isEmpty()) {
			throw new ServiceException("Id不能为空");
		}
		return editMapper.findEditBookById(id);
	}

	@Override
	public Note EditslectBookTxt(String notes_id) throws Exception {
		if (notes_id == null || notes_id.trim().isEmpty()) {
			throw new ServiceException("notes_id不能为空");
		}
		return editMapper.findEditBookTxtById(notes_id);
	}

	@Override
	public void EditUpdateBookTxt(Note note) throws Exception {
		if (note.getNotesId() == null || note.getNotesId().trim().isEmpty()) {
			throw new ServiceException("notes_id不能为空");
		}
		editMapper.updateEditBookTxtById(note);
	}

	@Override
	public void EditaddNotBook(String title, String notebookId, String userId)
			throws Exception {
		if (title == null || title.trim().isEmpty()) {
			title = "未命名笔记";
		}
		if (notebookId == null || notebookId.trim().isEmpty()) {
			new ServiceException("必须选择笔记本");
		}
		Note notes = new Note();
		notes.setUser_id(userId);
		notes.setBook_id(notebookId);
		notes.setNotes_tite(title);
		System.out.println(notes);
		editMapper.addNotBook(notes);
	}

	@Override
	public void EditaddNotBooks(String user_id, String book_name)
			throws Exception {
		if (book_name == null || book_name.trim().isEmpty()) {
			book_name = "未命名笔记本";
		}
		NoteBook notebook = new NoteBook();
		notebook.setUserId(user_id);
		notebook.setBookName(book_name);
		editMapper.addNotBooks(notebook);
	}
}