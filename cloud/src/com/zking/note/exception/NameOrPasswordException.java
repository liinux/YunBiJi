package com.zking.note.exception;

/**
 * 自定义异常类
 * @author Administrator
 *
 */
public class NameOrPasswordException extends RuntimeException {
	private String message = null;
	
	public NameOrPasswordException(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
