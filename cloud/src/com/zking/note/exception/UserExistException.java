package com.zking.note.exception;

public class UserExistException extends RuntimeException {

	private String message = null;
    public UserExistException(String message) {
       this.message = message;
    }
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
