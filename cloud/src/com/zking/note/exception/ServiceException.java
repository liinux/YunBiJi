package com.zking.note.exception;

public class ServiceException extends RuntimeException{
	String message = null;
	public ServiceException(String message){
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
