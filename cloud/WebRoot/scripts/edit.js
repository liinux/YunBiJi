//模型对象
var model = {
	notebooks : [], // 笔记本列表
	currentNotebook : {}, // 当前选定的笔记本
	notes : [], // 当前笔记本的笔记列表
	currentNote : {}
// 当前正在编辑的笔记
};
var userId;
$(function() {
	// 页面加载以后，可以获取登录用户的id
	userId = getCookie("userId");
	console.log(userId);
	listNotebooksAction(userId);// 查询笔记本
	// 绑定添加事件
	$('#add_note').click(addNoteAction);
	$('#add_notebook').click(addNoteBookAction);
	$('.note_menu').click(show);
});

function listNotebooksAction(userId) {
	var url = "http://localhost:8080/cloud/edit/countnote.do?userId=" + userId;
	$.getJSON(url, function(result) {
		if (result.state == SUCCESS) {
			var list = result.data;
			for ( var i = 0; i < list.length; i++) {
				// console.log(list[i]);
			}
			// 更新数据模型
			model.notebooks = list;
			// 刷新笔记本列表的显示
			paintNotebooks();
		}
	});
}

function paintNotebooks() {
	var list = model.notebooks;
	var ul = $('#notebooks');
	ul.empty();
	for ( var i = 0; i < list.length; i++) {
		var notebook = list[i];
		var li = '<li class="online" id="'
				+ notebook.BOOK_ID
				+ '" onclick="onlineclic(this.id)">'
				+ '<a>'
				+ '<i class="fa fa-book" title="online" rel="tooltip-bottom"></i>'
				+ notebook.BOOK_NAME + '</a>' + '</li>';
		ul.append(li);
	}

}
var id;
function onlineclic(ids) {
	id = ids;
	var url = "http://localhost:8080/cloud/edit/countnotetype.do?book_id=" + id;
	$.getJSON(url, function(result) {
		if (result.state == SUCCESS) {
			var booklist = result.data;
			for ( var i = 0; i < booklist.length; i++) {
				// console.log(list[i]);
			}
			// 更新数据模型
			model.notes = booklist;
			// 刷新笔记本列表的显示
			paintbooks();
		}
	});
}
function paintbooks() {
	var booklist = model.notes;
	var ul = $('#notes');
	ul.empty();
	for ( var i = 0; i < booklist.length; i++) {
		var notes = booklist[i];
		var li = '<li class="online" id="'
				+ notes.NOTES_ID
				+ '" onclick="onlinetext(this.id)">'
				+ '<a>'
				+ '<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i>'
				+ notes.NOTES_TITE
				+ '<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down"><i class="fa fa-chevron-down"></i></button>'
				+ '</a>'
				+ '<div class="note_menu" tabindex="-1">'
				+ '<dl>'
				+ '<dt><button type="button" class="btn btn-default btn-xs btn_move" title="移动至..."><i class="fa fa-random"></i></button></dt>'
				+ '<dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt>'
				+ '<dt><button type="button" class="btn btn-default btn-xs btn_delete" title="删除"><i class="fa fa-times"></i></button></dt>'
				+ '</dl>' + '</div>' + '</li>';

		ul.append(li);
	}
}
// 把notes_id定义出来用来下面data中传值
var txtId;
function onlinetext(txtid) {
	var url = "http://localhost:8080/cloud/edit/countnotetxt.do?notes_id="
			+ txtid;
	txtId = txtid
	var a = $("#input_note_title").val("笔记加载中...");
	$.getJSON(url, function(result) {
		if (result.state == SUCCESS) {
			var booktxtlist = result.data;
			// 更新数据模型
			model.currentNote = booktxtlist;
			paintbooktxt();
		} else {
			alert(result.message);
		}
	});
}
function paintbooktxt() {
	var booktxtlist = model.currentNote;
	// 往标题塞值
	$("#input_note_title").val(booktxtlist.notes_tite);
	// 往文本框塞值
	if (um.getContent(booktxtlist.notes_content) != "") {
		um.setContent("");
	}
	um.setContent(booktxtlist.notes_content);
}

$(function() {
	$("#save_note").click(function() {
		var booktxt = model.currentNote;
		var title = $("#input_note_title").val();
		var body = um.getContent();
		// 判断文本框以及标题框里面内容是否相同，如相同则return,如不同就发送AJAX请求进行修改及保存
		/*
		 * if (title == booktxt.notes_tite && body == booktxt.notes_content) { }
		 */
		// 定义好AJAX中要传到后台的值
		var data = {
			'txtid' : txtId,
			'title' : title,
			'body' : body
		};
		// 定义好请求路径
		var url = "http://localhost:8080/cloud/edit/updateNote.do";
		// 将以上定义好的信息通过AJAX发送到后台
		$('#save_note').html('保存中...').attr('disabled', 'disabled');
		$.post(url, data, function(result) {
			$('#save_note').html('保存笔记').removeAttr('disabled');
			;
			/*
			 * var note = result.data; model.currentNote = note;
			 * //listCurrentNotesAction();
			 * model.notes[model.noteIndex].title=note.title;
			 */
			/* paintbooks(); */
			window.location.href = window.location.href;
		});
	});
});
function addNoteAction() {
	// 打开添加对话框
	var url = 'http://localhost:8080/cloud/alert/alert_note.html?t='
			+ (new Date).getTime();
	$('#can').load(url, function(response, status, xhr) {
		if (status == "success") {
			// 绑定按钮事件
			$('#can .cancel, #can .close').click(function() {
				console.log("click");
				$('#can > div').hide(200, function() {
					$('#can').empty();
				});
			});
			// 当按钮点为确定时，触发点击事件，调用saveNewNoteAction方法
			$('#can .sure').click(saveNewNoteAction);
			$('#can').show(500);
			$('#can > div').show(200);
		} else {
			$('#can').empty();
		}
	});
	return false;
}
function saveNewNoteAction() {
	// 获取添加到输入框里面内容
	var title = $('#input_note').val();
	// 从cookie中获取到当前用户id
	var userId = getCookie("userId");
	date = {
		title : title,
		userId : userId,
		book_id : id
	};
	var url = "http://localhost:8080/cloud/edit/addNote.do";
	if (id == null) {
		alert("必须选择笔记本")
		return;
	}
	$.post(url, date, function(result) {
		window.location.href = window.location.href;
	});
}
function addNoteBookAction() {
	// 打开添加对话框
	var url = 'http://localhost:8080/cloud/alert/alert_notebook.html?t='
			+ (new Date).getTime();
	$('#can').load(url, function(response, status, xhr) {
		if (status == "success") {
			// 绑定按钮事件
			$('#can .cancel, #can .close').click(function() {
				console.log("click");
				$('#can > div').hide(200, function() {
					$('#can').empty();
				});
			});
			// 当按钮点为确定时，触发点击事件，调用saveNewNoteAction方法
			$('#can .sure').click(saveNewNoteBookAction);
			$('#can').show(500);
			$('#can > div').show(200);
		} else {
			$('#can').empty();
		}
	});
	return false;
}
function saveNewNoteBookAction(){
	// 获取添加到输入框里面内容
	var title = $('#input_notebook').val();
	// 从cookie中获取到当前用户id
	var userId = getCookie("userId");
	date={title:title,userId:userId};
	var url = "http://localhost:8080/cloud/edit/addNoteBook.do";
	$.post(url,date,function(){
		listNotebooksAction(userId);
	});
}
function show(){
	alert("sd");
}