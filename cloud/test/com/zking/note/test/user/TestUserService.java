package com.zking.note.test.user;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.zking.note.pojo.User;
import com.zking.note.service.EditService;
import com.zking.note.service.UserService;

public class TestUserService {
	private ApplicationContext ac;

	private UserService userService;

	@Before
	// 启动容器
	public void setUp() {
		this.ac = new ClassPathXmlApplicationContext(
				"classpath:spring/spring-*.xml");
	}

	// @Test
	public void testLogin() throws Exception {
		userService = (UserService) ac.getBean("userService");
		User user = userService.login("tom", "123");
		System.out.println(user);
	}

	@Test
	public void EditUpdateBookTxt() throws Exception {
		EditService editSer = (EditService) ac.getBean("editServiceImp");
		editSer.EditaddNotBooks( "1", "红楼梦");
		
		
	}
}
