package com.zking.note.test.user;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.zking.note.mapper.EditMapper;
import com.zking.note.mapper.UserMapper;
import com.zking.note.pojo.Note;
import com.zking.note.pojo.NoteBook;
import com.zking.note.pojo.User;

public class TestUserMapper {

	private ApplicationContext ac;

	@Before
	// 启动容器
	public void setUp() {
		this.ac = new ClassPathXmlApplicationContext(
				"classpath:spring/spring-*.xml");
	}

	@Test
	public void addNotBook() throws Exception {
		EditMapper mapper = (EditMapper) ac.getBean("editMapper");
		NoteBook nb=new NoteBook();
		nb.setUserId("1");
		nb.setBookName("hehe");
		 mapper.addNotBooks(nb);

	}

	// @Test
	public void testFindUserById() throws Exception {

		UserMapper mapper = (UserMapper) ac.getBean("userMapper");

		User user = mapper.findUserById("003");

		System.out.println(user);
	}

	// @Test
	public void testFindUserByName() throws Exception {

		UserMapper mapper = (UserMapper) ac.getBean("userMapper");

		User user = mapper.findUserByName("狗娃");

		System.out.println(user);
	}
}
